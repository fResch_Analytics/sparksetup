#!/bin/bash

# First step is to fetch the hadoop binary
echo "Retrieving hadoop."
curl http://apache.cs.utah.edu/hadoop/common/hadoop-2.7.7/hadoop-2.7.7.tar.gz --output hadoop-2.7.7.tar.gz

# Extract the hadoop
echo "Extracting hadoop."
tar xvzpf hadoop-2.7.7.tar.gz

# Move to /opt
echo "Moving hadoop."
sudo mv hadoop-2.7.7 /opt/

# echo "" >> ~/.bashrc
# echo "#Adding hadoop to the path" >> ~/.bashrc
# echo "export PATH='$PATH:/opt/hadoop-2.7.7/bin'" >> ~/.bashrc

echo "" >> ~/.bashrc
echo "export HADOOP_HOME=/opt/hadoop-2.7.7" >>~/.bashrc
source ~/.bashrc

# Clean up
echo "Cleaning up."
rm hadoop-2.7.7.tar.gz