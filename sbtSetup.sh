#!/bin/bash

# First step is to fetch the sbt binary
echo "Retrieving sbt."
curl https://sbt-downloads.cdnedge.bluemix.net/releases/v1.3.5/sbt-1.3.5.tgz --output sbt-1.3.5.tgz

# Extract the sbt
echo "Extracting sbt."
tar xvzpf sbt-1.3.5.tgz

# Move to /opt
echo "Moving sbt."
sudo mv sbt /opt/

echo "#Adding sbt to the path" >> ~/.bashrc
echo "export PATH='$PATH:/opt/sbt/bin'" >> ~/.bashrc
source ~/.bashrc

# Clean up
echo "Cleaning up."
rm sbt-1.3.5.tgz