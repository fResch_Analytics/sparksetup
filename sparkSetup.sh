
#!/bin/bash

# First step is to fetch the spark binary
echo "Retrieving spark."
curl http://apache.cs.utah.edu/spark/spark-2.4.4/spark-2.4.4-bin-hadoop2.7.tgz --output spark-2.4.4.tgz

# Extract the spark
echo "Extracting spark."
tar xvzpf spark-2.4.4.tgz

# Move to /opt
echo "Moving spark."
sudo mv spark-2.4.4-bin-hadoop2.7 /opt/spark-2.4.4

echo "" >> ~/.bashrc
echo "export SPARK_HOME=/opt/spark-2.4.4" >>~/.bashrc
source ~/.bashrc

echo "" >> ~/.bashrc
echo "#Adding spark to the path" >> ~/.bashrc
echo "export PATH='$PATH:/opt/hadoop-2.7.7/bin:/opt/spark-2.4.4/bin'" >> ~/.bashrc

# Clean up
echo "Cleaning up."
rm spark-2.4.4.tgz